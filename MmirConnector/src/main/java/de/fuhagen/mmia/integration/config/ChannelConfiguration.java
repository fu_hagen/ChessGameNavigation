package de.fuhagen.mmia.integration.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.MessageChannel;

/**
 * Configuration class for defining Spring Integration message channels.
 * It provides configuration for creating message channels used for communication
 * between various components in the application.
 */
@Configuration
public class ChannelConfiguration {

    /** Header key for user ID information in messages. */
    public static final String HEADER_USER_ID = "UserID";
    
    /**
     * Creates a message channel for receiving Kafka messages.
     *
     * @return A QueueChannel configured for handling String messages.
     */
    @Bean(name = "kafka-in-channel")
    MessageChannel kafkaInChannel() {
        QueueChannel queueChannel = new QueueChannel();
        queueChannel.setDatatypes(String.class);
        return queueChannel;
    }

    /**
     * Creates a message channel for outgoing media messages.
     *
     * @return A DirectChannel configured for handling Boolean messages.
     */
    @Bean(name = "kafka-out-channel")
    MessageChannel kafkaOutChannel() {
            DirectChannel directChannel = new DirectChannel();
            directChannel.setDatatypes(Boolean.class);
            return directChannel;
    }  
}
