package de.fuhagen.mmia.integration.flow;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.messaging.MessageHeaders;
import org.springframework.web.client.RestClient;

import de.fuhagen.mmia.integration.config.ChannelConfiguration;
import de.fuhagen.mmia.message.broker.publisher.MsgBrokerPublisher;
import de.fuhagen.mmia.message.restclient.GmafClient;
import de.fuhagen.mmia.message.samba.FileserverConnector;

/**
 * Configuration class defining Spring Integration flows for message processing.
 * It provides configuration for processing messages received from Kafka and handling responses.
 */
@Configuration
@EnableIntegration
public class FlowDefinitions {

    private static final String MULTIMEDIA_RETRIEVE_SUCCESS = "success";
    private static final String MULTIMEDIA_RETRIEVE_FAIL = "fail";

    private MsgBrokerPublisher msgBrokerPublisher;

    @Autowired
    public void setMsgBrokerPublisher (MsgBrokerPublisher msgBrokerPublisher) {
        this.msgBrokerPublisher = msgBrokerPublisher;
    }
    
    /**
     * Defines an integration flow for processing Kafka messages and uploading multimedia content to a file server.
     *
     * @return An IntegrationFlow definition for handling Kafka messages and multimedia uploads.
     */
    @Bean
    public IntegrationFlow kafkaToFileserverFlow() {
        return IntegrationFlow
                .from("kafka-in-channel")
                .handle(
                    String.class,
                    (String payload, MessageHeaders header) -> {
                        GmafClient gmafClient = new GmafClient(payload, RestClient.create());
                        try {
                            ByteArrayOutputStream gmafResult = gmafClient.execute();
                            FileserverConnector.uploadMultiMedia((int) header.get(ChannelConfiguration.HEADER_USER_ID), gmafResult);
                            return true;
                        } 
                        catch (RuntimeException e) {
                            e.printStackTrace();
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        } 
                        return false;
                    } 
                )
                .channel("kafka-out-channel")
                .get();
    }

    /**
     * Defines an integration flow for processing file server responses and sending them to Kafka.
     *
     * @return An IntegrationFlow definition for handling file server responses and sending them to Kafka.
     */
    @Bean
    public IntegrationFlow fileserverResponseFlow() {
        return IntegrationFlow
                .from("kafka-out-channel")
                .handle(
                    Boolean.class,
                    (Boolean payload, MessageHeaders header) -> {
                        System.out.println("mediaToFileserverFlow: " + ChannelConfiguration.HEADER_USER_ID + ": " + header.get(ChannelConfiguration.HEADER_USER_ID));
                        msgBrokerPublisher.sendToKafka(
                            (int) header.get(ChannelConfiguration.HEADER_USER_ID), 
                            payload ? MULTIMEDIA_RETRIEVE_SUCCESS : MULTIMEDIA_RETRIEVE_FAIL
                        );
                        return true; // return anything for choosing a specify Service Activator. 
                    } 
                )
                .channel("nullChannel")
                .get();
    }  
}
