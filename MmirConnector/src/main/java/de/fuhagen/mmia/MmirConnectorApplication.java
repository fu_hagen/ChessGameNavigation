package de.fuhagen.mmia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The main entry point for the MmirConnector application.
 * This class contains the `main` method that initializes and starts the Spring Boot application.
 *
 * The `@SpringBootApplication` annotation marks this class as the starting point for the Spring Boot application,
 * enabling auto-configuration and component scanning.
 */
@SpringBootApplication
public class MmirConnectorApplication {

	/**
     * The main method to launch the MmirConnector Spring Boot application.
     *
     * @param args Command-line arguments (if any).
     */
	public static void main(String[] args) {
		SpringApplication.run(MmirConnectorApplication.class, args);
	}

}
