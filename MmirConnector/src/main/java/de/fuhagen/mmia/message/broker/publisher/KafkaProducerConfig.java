package de.fuhagen.mmia.message.broker.publisher;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

/**
 * Configuration class for setting up Kafka producer properties and the KafkaTemplate.
 * It provides configuration for creating a Kafka producer factory and configuring the KafkaTemplate.
 * The producer factory is used to create Kafka producers, and the KafkaTemplate is used to send messages to Kafka topics.
 *
 * This configuration class specifies the serialization and bootstrap server properties for Kafka producers.
 */
@Configuration
public class KafkaProducerConfig {
    
    private static final String BOOT_STRAP_ADDRESS = "kafka:9092";

    /**
     * Creates and configures a Kafka Producer Factory for String key and value serialization.
     *
     * @return The configured Kafka Producer Factory.
     */
    @Bean
    public ProducerFactory<String, String> producerFactory() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOT_STRAP_ADDRESS);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    } 

    /**
     * Creates a KafkaTemplate for sending messages to Kafka topics.
     *
     * @return The configured KafkaTemplate.
     */
    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }
}
