package de.fuhagen.mmia.message.broker.listener;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

/**
 * Configuration class for setting up Kafka consumer properties and the Kafka listener container factory. 
 * It provides configuration for creating a Kafka consumer factory and configuring the Kafka listener container factory. 
 * The consumer factory is used to create Kafka consumers, and the listener container factory is used to create listener containers for consuming Kafka messages.
 */
@Configuration
public class KafkaConsumerConfig {

    private static final String BOOT_STRAP_ADDRESS = "kafka:9092";
    
    /**
     * Creates and configures a Kafka Consumer Factory for String key and value deserialization.
     *
     * @return The configured Kafka Consumer Factory.
     */
    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOT_STRAP_ADDRESS);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return new DefaultKafkaConsumerFactory<>(props);
    }

    /**
     * Creates a Concurrent Kafka Listener Container Factory for String key and value types.
     *
     * @return The configured Concurrent Kafka Listener Container Factory.
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String>
    kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

}
