package de.fuhagen.mmia.message.broker.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import de.fuhagen.mmia.integration.config.ChannelConfiguration;
import de.fuhagen.mmia.message.broker.listener.functionalinterface.KafkaIncomeMsg;

/**
 * This component serves as a gateway for processing incoming messages and forwarding them to a Kafka message channel.
 * It is responsible for creating a functional interface to handle incoming messages and interacts with the {@link MsgBrokerListener} to set the income message handler.
 */
@Component
public class IncomeMsgGateway {
    
    private MsgBrokerListener msgBrokerListener;
    
    private MessageChannel kafkaInChannel;

    @Autowired
    public void setKafkaInChannel (@Qualifier("kafka-in-channel") MessageChannel kafkaInChannel) {
        this.kafkaInChannel = kafkaInChannel;
    }

    private KafkaIncomeMsg incomeMsg;

    /**
     * Constructs an instance of the IncomeMsgGateway class.
     *
     * @param msgBrokerListener The message broker listener responsible for processing incoming messages.
     */
    public IncomeMsgGateway(@Autowired MsgBrokerListener msgBrokerListener){
        this.createFunctionObjects();   
        this.msgBrokerListener = msgBrokerListener;
        this.msgBrokerListener.setIncomeMsg(incomeMsg);
    }

    /**
     * Creates a functional object to handle incoming Kafka messages and forward them to the Kafka message channel.
     */
    private void createFunctionObjects() {
        incomeMsg = (final int key, final String msg) -> {
            Message<String> message = MessageBuilder.withPayload(msg).setHeader(ChannelConfiguration.HEADER_USER_ID, key).build();
            kafkaInChannel.send(message);
        };
    }
}
