package de.fuhagen.mmia.message.broker.publisher;

import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

/**
 * A component responsible for publishing messages to a Kafka topic.
 * It provides a method to send messages to the "multimedia-retrieval-response" Kafka topic.
 *
 * This component uses a KafkaTemplate for sending messages asynchronously to the specified Kafka topic.
 * It handles the serialization of messages as JSON objects before sending them to Kafka.
 * Any errors encountered during message publishing are logged.
 */
@Component
public class MsgBrokerPublisher {

    private static final String KEY_ID = "id";
    private static final String KEY_MESSAGE = "message";
    private static final String TOPIC_NAME = "multimedia-retrieval-response";

    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public void setKafkaTemplate (KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    /**
     * Sends a message to the "multimedia-retrieval-response" Kafka topic with the specified key and message content.
     *
     * @param key The integer key associated with the message.
     * @param msg The message content as a string.
     */
    public void sendToKafka(final int key, final String msg) {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put(KEY_ID, key);
        jsonObj.put(KEY_MESSAGE, msg);
        CompletableFuture<SendResult<String, String>> future = kafkaTemplate.send(TOPIC_NAME, jsonObj.toString());
        future.whenComplete(
            (SendResult<String, String> result, Throwable ex) -> {
                if (ex != null) {
                    System.err.println(
                        new StringBuilder("The message ").append(jsonObj.toString()).append(" cannot be sent.").append(" Because ").append(ex.getMessage())
                    );
                } 
            } 
        );
    } 
}
