package de.fuhagen.mmia.message.samba;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import com.hierynomus.msdtyp.AccessMask;
import com.hierynomus.msfscc.FileAttributes;
import com.hierynomus.mssmb2.SMB2CreateDisposition;
import com.hierynomus.mssmb2.SMB2CreateOptions;
import com.hierynomus.mssmb2.SMB2ShareAccess;
import com.hierynomus.mssmb2.SMBApiException;
import com.hierynomus.smbj.SMBClient;
import com.hierynomus.smbj.auth.AuthenticationContext;
import com.hierynomus.smbj.connection.Connection;
import com.hierynomus.smbj.session.Session;
import com.hierynomus.smbj.share.DiskShare;
import com.hierynomus.smbj.share.File;
import com.hierynomus.smbj.utils.SmbFiles;

/**
 * A class for uploading multimedia files to a Samba file server.
 * It provides a method to upload a multimedia file to a specific folder on the Samba server.
 *
 * This class utilizes the SMBJ library for interacting with the Samba file server using the SMB/CIFS protocol.
 * It establishes a connection, authenticates the user, and uploads the file to the specified folder.
 */
public class FileserverConnector {
    
    private static final String DOMAIN = "WORKGROUP";
    private static final String SERVER_NAME = "samba";
    private static final String SHARE_NAME = "public";
    private static final String SAMBA_USER = "sambauser";
    private static final String SAMBA_PASSWORD = "samba";
    private static final String VIDEO_FILE_NAME = "video.mp4";

    /**
     * Uploads a multimedia file to the specified folder on the Samba file server.
     *
     * @param folderId   The ID of the folder where the file should be uploaded.
     * @param fileStream The ByteArrayOutputStream containing the multimedia file data.
     * @throws IOException    If there is an I/O error during the file upload.
     * @throws SMBApiException If there is an error in the SMB API during the upload process.
     */
    public static void uploadMultiMedia(final int folderId, ByteArrayOutputStream fileStream) throws IOException, SMBApiException {
        SMBClient client = new SMBClient();

        try (Connection connection = client.connect(SERVER_NAME)) {
            Session session = connection.authenticate(
                new AuthenticationContext(SAMBA_USER, SAMBA_PASSWORD.toCharArray(), DOMAIN)
            );
            try (DiskShare diskShare = (DiskShare) session.connectShare(SHARE_NAME)) {
                SmbFiles smbFiles = new SmbFiles();
                smbFiles.mkdirs(diskShare, String.valueOf(folderId));

                Set<FileAttributes> fileAttributes = new HashSet<>();
                fileAttributes.add(FileAttributes.FILE_ATTRIBUTE_NORMAL);
                Set<SMB2CreateOptions> createOptions = new HashSet<>();
                createOptions.add(SMB2CreateOptions.FILE_RANDOM_ACCESS);
                File file = diskShare.openFile(
                    new StringBuilder(String.valueOf(folderId)).append(java.io.File.separator).append(VIDEO_FILE_NAME).toString(), 
                    EnumSet.of(AccessMask.GENERIC_ALL), 
                    fileAttributes, 
                    SMB2ShareAccess.ALL, 
                    SMB2CreateDisposition.FILE_OVERWRITE_IF, 
                    createOptions
                );

                OutputStream outputStream = file.getOutputStream();
                fileStream.writeTo(outputStream);
                outputStream.flush();
                outputStream.close();

                client.close();
            } 
        }
    } 
}
