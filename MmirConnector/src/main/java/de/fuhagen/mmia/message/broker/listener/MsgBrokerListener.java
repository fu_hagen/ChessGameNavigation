package de.fuhagen.mmia.message.broker.listener;

import org.json.JSONObject;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import de.fuhagen.mmia.message.broker.listener.functionalinterface.KafkaIncomeMsg;

/**
 * A message broker listener component that consumes messages from a Kafka topic and delegates processing to the configured KafkaIncomeMsg functional interface.
 * It listens to the "multimedia-retrieval" Kafka topic and extracts key and message information from incoming JSON messages.
 * This component is responsible for handling incoming messages and passing them to the configured message handler.
 */
@Component
public class MsgBrokerListener {

    private KafkaIncomeMsg incomeMsg;
    private static final String KEY_ID = "id";
    private static final String KEY_MESSAGE = "message";

    /**
     * Kafka listener method that consumes messages from the "multimedia-retrieval" topic and delegates processing to the configured message handler.
     *
     * @param jsonMsg The JSON message received from the Kafka topic.
     * @throws RuntimeException If the incomeMsg functional interface is not set.
     */
    @KafkaListener(id = "MmirConnector", topics = "multimedia-retrieval")
    public void listen(@Payload String jsonMsg) {
        if ( incomeMsg == null ) {
            throw new RuntimeException("incomeMsg is not set yet!");
        }
        JSONObject jsonObj = new JSONObject(jsonMsg);
        incomeMsg.retrieveMsg(jsonObj.getInt(KEY_ID), jsonObj.getString(KEY_MESSAGE));
    }

    /**
     * Sets the incomeMsg functional interface to be used for message processing.
     *
     * @param incomeMsg The functional interface for processing incoming messages.
     */
    public void setIncomeMsg(final KafkaIncomeMsg incomeMsg) {
        this.incomeMsg = incomeMsg;
    }

}
