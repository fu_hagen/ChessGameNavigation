package de.fuhagen.mmia.message.restclient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestClientResponseException;

/**
 * A client class for interacting with the GMAF (Generic Multimedia Analysis Framework) REST API.
 * This class is responsible for executing a sequence of API requests to obtain multimedia assets
 * based on a given query.
 *
 * It supports retrieving an authentication token, searching for multimedia assets by a query,
 * and downloading multimedia content associated with a specific asset ID.
 */
public class GmafClient {

    private static final String ADDRESS_PORT_SEPARATOR = ":";
    private static final String LOCATION_SEPARATOR = "/";
    private static final String GMAF_REST_ENDPOINT = "gmaf/gmafApi/gmaf";
    private static final String GMAF_TOKEN_PATH_VAR = "getToken";
    private static final String GMAF_QUERY_LOCATION = "query";
    private static final String GMAF_MULTIMEDIA_LOCATION = "preview";

    private static final String apiKey = System.getenv("API_KEY");
    private static final String gmafAdress = System.getenv("GMAF_ADDRESS");
    private static final String gmafPort = System.getenv("GMAF_PORT");


    private String query;
    private RestClient client;

    /**
     * Constructs a GmafClient instance with the specified query and RestClient.
     *
     * @param query  The query used to search for multimedia assets.
     * @param client The RestClient for making HTTP requests.
     */
    public GmafClient(final String query, RestClient client) {
        this.query = query;
        this.client = client;
    }

    /**
     * Executes a sequence of API requests to obtain multimedia assets based on the provided query.
     *
     * @return A ByteArrayOutputStream containing the downloaded multimedia content.
     * @throws RuntimeException If there is an error during the API requests or if the status code is not OK.
     * @throws IOException       If there is an I/O error during the download.
     */
    public ByteArrayOutputStream execute() throws RuntimeException, IOException {
        try {
            String gmafToken = getToken();
            String mmfgId = searchByQuery(gmafToken);
            ByteArrayOutputStream result = downloadMultiMedia(gmafToken, mmfgId);

            return result;
        } 
        catch (RuntimeException e) {
            throw e;
        }
        catch (IOException e) {
            throw e;
        }
    }

    private String getToken() throws RuntimeException{

        String url = new StringBuilder(gmafAdress).append(ADDRESS_PORT_SEPARATOR).append(gmafPort).append(LOCATION_SEPARATOR).append(GMAF_REST_ENDPOINT).append(LOCATION_SEPARATOR).append(GMAF_TOKEN_PATH_VAR).append(LOCATION_SEPARATOR).append(apiKey).toString();

        try {
            ResponseEntity<String> result = client
                                .get()
                                .uri(url)
                                .retrieve()
                                .toEntity(String.class);
            
            if (result.getStatusCode() != HttpStatus.OK) {
                throw new RuntimeException(
                    new StringBuilder("GmafClient::getToken(): Status code is ").append(result.getStatusCode().toString()).toString()
                );
            } 

            return result.getBody();
        } 
        catch (RestClientResponseException e) {
            throw e;
        }
    }  

    private String searchByQuery(final String authToken) throws RuntimeException {

        String url = new StringBuilder(gmafAdress).append(ADDRESS_PORT_SEPARATOR).append(gmafPort).append(LOCATION_SEPARATOR).append(GMAF_REST_ENDPOINT).append(LOCATION_SEPARATOR).append(GMAF_QUERY_LOCATION).append(LOCATION_SEPARATOR).append(authToken).append(LOCATION_SEPARATOR).append(query).toString();

        try{
            // somehow GMAF provides for searching use case a POST API instead of a GET API. However this function does not aim to push information to GMAF ...
            ResponseEntity<String[]> postResponse = client
                                        .post()
                                        .uri(url)
                                        .retrieve()
                                        .toEntity(String[].class);
            if (postResponse.getStatusCode() != HttpStatus.OK){
                throw new RuntimeException(
                    new StringBuilder("GmafClient::searchByQuery(): Status code is ").append(postResponse.getStatusCode()).toString()
                );
            }

            // return the first result of the search list since results are sorted by similarity.
            // sort algorithm is implemented in GMAF
            return postResponse.getBody()[0];
        } 
        catch (RestClientResponseException e) {
            throw e;
        } 
    }

    private ByteArrayOutputStream downloadMultiMedia(final String authToken, final String mmfgId) throws RuntimeException, IOException {
        String url = new StringBuilder(gmafAdress).append(ADDRESS_PORT_SEPARATOR).append(gmafPort).append(LOCATION_SEPARATOR).append(GMAF_REST_ENDPOINT).append(LOCATION_SEPARATOR).append(GMAF_MULTIMEDIA_LOCATION).append(LOCATION_SEPARATOR).append(authToken).append(LOCATION_SEPARATOR).append(mmfgId).toString();

        try {
            ResponseEntity<byte[]> getResponse = client
                                        .get()
                                        .uri(url)
                                        .retrieve()
                                        .toEntity(byte[].class);
            if (getResponse.getStatusCode() != HttpStatus.OK) {
                throw new RuntimeException(
                    new StringBuilder("GmafClient::downloadMultiMedia(): Status code is ").append(getResponse.getStatusCode()).toString()
                ); 
            } 
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            result.write(getResponse.getBody());
            return result;
        } 
        catch (RestClientResponseException e) {
            throw e;
        }
        catch (IOException e) {
            throw e;
        }  
    } 
}
