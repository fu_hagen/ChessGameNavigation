package de.fuhagen.mmia.message.broker.listener.functionalinterface;

/**
 * Functional interface representing a callback for processing incoming Kafka messages.
 * Implementations of this interface should provide a method to retrieve and handle
 * Kafka messages with a given integer key and a message string.
 */
@FunctionalInterface
public interface KafkaIncomeMsg {

    /**
     * Retrieves and processes a Kafka message with the specified key and message content.
     *
     * @param key The integer key associated with the Kafka message.
     * @param msg The message content received from Kafka as a string.
     */
    public void retrieveMsg(final int key, final String msg);
}
