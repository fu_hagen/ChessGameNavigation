package de.fuhagen.mmia.integration.message.restclient;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClient;

import de.fuhagen.mmia.message.restclient.GmafClient;

public class GmafClientTest {

    private static final String ADDRESS_PORT_SEPARATOR = ":";
    private static final String LOCATION_SEPARATOR = "/";
    private static final String GMAF_REST_ENDPOINT = "gmaf/gmafApi/gmaf";
    private static final String GMAF_TOKEN_PATH_VAR = "getToken";
    private static final String GMAF_QUERY_LOCATION = "query";
    private static final String GMAF_MULTIMEDIA_LOCATION = "preview";

    private static final String apiKey = System.getenv("API_KEY");
    private static final String gmafAdress = System.getenv("GMAF_ADDRESS");
    private static final String gmafPort = System.getenv("GMAF_PORT");

    private static final String GMAF_TOKEN = "gmaf-token";
    private static final String GMAF_QUERY = "gmaf-query";
    private static final String[] MMFG_IDS = {"mmfg-id1", "mmfg-id2"};
    private static final byte[] VIDEO_FAKE_DATA = {1, 2, 3, 4}; 
    
    @Test 
    public void testGmafRequest() {
        RestClient mockedClient = Mockito.mock(RestClient.class, Mockito.RETURNS_DEEP_STUBS);

        Mockito.when(
            mockedClient
                .get()
                .uri(
                    new StringBuilder(gmafAdress).append(ADDRESS_PORT_SEPARATOR).append(gmafPort).append(LOCATION_SEPARATOR).append(GMAF_REST_ENDPOINT).append(LOCATION_SEPARATOR).append(GMAF_TOKEN_PATH_VAR).append(LOCATION_SEPARATOR).append(apiKey).toString()
                )
                .retrieve()
                .toEntity(String.class)
        )
        .thenReturn(new ResponseEntity<String>(GMAF_TOKEN, HttpStatus.OK));

        Mockito.when(
            mockedClient
                .post()
                .uri(
                    new StringBuilder(gmafAdress).append(ADDRESS_PORT_SEPARATOR).append(gmafPort).append(LOCATION_SEPARATOR).append(GMAF_REST_ENDPOINT).append(LOCATION_SEPARATOR).append(GMAF_QUERY_LOCATION).append(LOCATION_SEPARATOR).append(GMAF_TOKEN).append(LOCATION_SEPARATOR).append(GMAF_QUERY).toString()
                )
                .retrieve()
                .toEntity(String[].class)
        ).thenReturn(new ResponseEntity<String[]>(MMFG_IDS, HttpStatus.OK));

        Mockito.when(
            mockedClient
            .get()
            .uri(
                new StringBuilder(gmafAdress).append(ADDRESS_PORT_SEPARATOR).append(gmafPort).append(LOCATION_SEPARATOR).append(GMAF_REST_ENDPOINT).append(LOCATION_SEPARATOR).append(GMAF_MULTIMEDIA_LOCATION).append(LOCATION_SEPARATOR).append(GMAF_TOKEN).append(LOCATION_SEPARATOR).append(MMFG_IDS[0]).toString()
            )
            .retrieve()
            .toEntity(byte[].class)
        ).thenReturn(new ResponseEntity<byte[]>(VIDEO_FAKE_DATA, HttpStatus.OK));

        GmafClient gmafClient = new GmafClient(GMAF_QUERY, mockedClient);

        ByteArrayOutputStream expected = new ByteArrayOutputStream();
        try {
            expected.write(VIDEO_FAKE_DATA);
            ByteArrayOutputStream result = gmafClient.execute();
            byte[] byteArrayExpected = expected.toByteArray();
            byte[] byteArrayResult = result.toByteArray();
            assertTrue(Arrays.equals(byteArrayExpected, byteArrayResult));
        } catch (IOException e) {
            fail(e.getMessage());
        }
    } 

}
