# Chess Game Navigation
- [Introduction](#introduction)
- [Architecture of the POC](#architecture-of-the-poc)
- [System Compatibility](#system-compatibility)
- [Build the POC](#build-the-poc)
- [Run the POC](#run-the-poc)
- [Accessing the POC](#accessing-the-poc)
- [How to Play](#how-to-play)
- [REST API Documentation](#rest-api-documentation)
## Introduction
This proof of concept (POC) resulted from an internship in the field of Multimedia Information Retrieval at the University of Hagen, focusing on knowledge transfer in virtual worlds. The goal of this internship was to develop a web service that simulates a navigator in a virtual world to handle user requests, including retrieving and providing multimedia information from the [Generic Multimedia Analysis Framework (GMAF)](https://github.com/stefanwagenpfeil/gmaf), a multimedia information retrieval system provided by the [Chair of Multimedia and Internet Applications of the University of Hagen](https://www.fernuni-hagen.de/multimedia-internetanwendungen/index.shtml).

This POC aims to help new chess players in learning how to play chess, as well as to support experienced players in learning new strategies. Since the design of a graphical interface to simulate the virtual environment was not required, the **user interface** was developed only at a **basic level** and is considered an additional feature rather than the primary focus (s. [Demo](#demo)). 

## Architecture of the POC
The POC is designed based on the [Microservices Architecture](), including the following components:
- [Web Application](https://gitlab.com/fu_hagen/ChessGameNavigation/-/tree/main/WebApplication?ref_type=heads): Act as the entry point for web clients
- [Kafka messages broker](https://kafka.apache.org/): Facilitates message communication between [Web Application](https://gitlab.com/fu_hagen/ChessGameNavigation/-/tree/main/WebApplication?ref_type=heads) and [MMIR Connector](https://gitlab.com/fu_hagen/ChessGameNavigation/-/tree/main/MmirConnector?ref_type=heads).
- [MMIR Connector](https://gitlab.com/fu_hagen/ChessGameNavigation/-/tree/main/MmirConnector?ref_type=heads): Integrates with the external [Generic Multimedia Analysis Framework (GMAF)](https://github.com/stefanwagenpfeil/gmaf) for retrieving multimedia data.
- [Samba file server](https://samba.org/): Manages file storage of multimedia data retrieved from [GMAF](https://github.com/stefanwagenpfeil/gmaf).
- [PostgreSQL database](https://www.postgresql.org/): Stores and manages data of web clients.

![architecture](./doc/architecture/System Architecture.svg){width=80%}


## System Compatibility

**Compatibility note**: Please see the [realease notes](https://gitlab.com/fu_hagen/ChessGameNavigation/-/releases).


## Build the POC
See [Web Application](https://gitlab.com/fu_hagen/ChessGameNavigation/-/tree/main/WebApplication?ref_type=heads) and [MMIR Connector](https://gitlab.com/fu_hagen/ChessGameNavigation/-/tree/main/MmirConnector?ref_type=heads).  


## Run the POC

To run this POC, [Docker](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/) have to be installed on the system. It is also possible to install [Docker Desktop](https://www.docker.com/products/docker-desktop/), which already contains Docker and Docker Compose. However, [Docker Desktop seems not to be very stable](https://github.com/docker/for-win/issues/8902).

Make sure that the [Docker Engine](https://docs.docker.com/engine/) is running.

Create the following `docker-compose.yml` file:

```yaml
---
version: "3"

services:
  zookeeper:
    image: 'confluentinc/cp-zookeeper:7.5.3'
    networks:
      - 'mmir_net'
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
  kafka:
    image: 'confluentinc/cp-kafka:7.5.3'
    volumes:
      - 'kafka_data:/var/lib/kafka'
    networks:
      - 'mmir_net'
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: 'zookeeper:2181'
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka:9092
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: '1'
    depends_on:
      zookeeper:
        condition: service_started
  init-kafka:
    image: 'confluentinc/cp-kafka:7.5.3'
    depends_on:
      kafka:
        condition: service_started
    networks:
      - 'mmir_net'
    entrypoint: [ '/bin/sh', '-c' ]
    command: |
      "
      # blocks until kafka is reachable
      kafka-topics --bootstrap-server kafka:9092 --list

      echo -e 'Creating kafka topics'
      kafka-topics --bootstrap-server kafka:9092 --create --if-not-exists --topic multimedia-retrieval --replication-factor 1 --partitions 1
      kafka-topics --bootstrap-server kafka:9092 --create --if-not-exists --topic multimedia-retrieval-response --replication-factor 1 --partitions 1
      echo -e 'Successfully created the following topics:'
      kafka-topics --bootstrap-server kafka:9092 --list
      "
  samba:
    image: 'ghcr.io/servercontainers/samba:smbd-only-a3.19.0-s4.18.9-r0'
    volumes:
      - 'samba_data:/shares'
    networks:
      - 'mmir_net'
    environment:
      ACCOUNT_sambauser: samba
      UID_sambauser: 1000
      SAMBA_VOLUME_CONFIG_public: '[Public]; path=/shares; valid users = sambauser; admin users = sambauser; guest ok = yes; read only = no; browseable = yes; writeable = yes; public = yes; create mask = 0644; directory mask = 0755'
  postgres:
    image: 'docker.io/postgres:16.1-bookworm'
    volumes:
      - "postgres_data:/var/lib/postgresql/data"
    networks:
      - mmir_net
    environment:
      POSTGRES_DB: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_USER: postgres  
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      interval: 5s
      timeout: 15s
      retries: 5   
  mmir_connector:
    image: '<mmir_connector_release_image>'
    networks:
      - mmir_net
    environment:
      API_KEY: '<api-key of gmaf>'
      GMAF_ADDRESS: 'http://<gmaf ip adress>'
      GMAF_PORT: '<gmaf port>'
    depends_on:
      kafka:
        condition: service_started
  web_app:
    image: '<web_app_release_image>'
    networks:
      - mmir_net
    ports:
      - '80:8080'
    depends_on:
      kafka:
        condition: service_started
      postgres:
        condition: service_healthy

networks:
  mmir_net:
    name: mmir_net
    driver: bridge

volumes:
  kafka_data:
    driver: local
  samba_data:
    driver: local
  postgres_data:
    driver: local
...
```

In order for the POC to work properly, the following environment variables have to be set:
- `API_KEY`: the api key to authenticate to GMAF.
- `GMAF_ADDRESS`: the IP adress of the GMAF instance.
- `GMAF_PORT`: the TCP port of the GMAF instance.

These informations are managed by the [Chair of Multimedia and Internet Applications of the University of Hagen](https://www.fernuni-hagen.de/multimedia-internetanwendungen/index.shtml).

In addition the `<web_app_release_image>` and `<mmir_connector_release_image>`values can be found on the [release page](https://gitlab.com/fu_hagen/ChessGameNavigation/-/releases).


If Docker Desktop is installed, run the following command:

```
docker compose up -d
```

If Docker and Docker Compose CLI is installed, run the following command:

```
docker-compose up -d
```

To shut down the POC, run the the following command:

```
docker compose down
```
or

```
docker-compose down
```


Note that for software developer, who want to explore this Chess Game Navigation deeper, can also use the following `docker-compose.yml` file to debug the POC:

```yaml
---
version: "3"

services:
  zookeeper:
    image: 'confluentinc/cp-zookeeper:7.5.3'
    networks:
      - 'mmir_net'
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
  kafka:
    image: 'confluentinc/cp-kafka:7.5.3'
    volumes:
      - 'kafka_data:/var/lib/kafka'
    networks:
      - 'mmir_net'
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: 'zookeeper:2181'
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka:9092
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: '1'
    depends_on:
      zookeeper:
        condition: service_started
  init-kafka:
    image: 'confluentinc/cp-kafka:7.5.3'
    depends_on:
      kafka:
        condition: service_started
    networks:
      - 'mmir_net'
    entrypoint: [ '/bin/sh', '-c' ]
    command: |
      "
      # blocks until kafka is reachable
      kafka-topics --bootstrap-server kafka:9092 --list

      echo -e 'Creating kafka topics'
      kafka-topics --bootstrap-server kafka:9092 --create --if-not-exists --topic multimedia-retrieval --replication-factor 1 --partitions 1
      kafka-topics --bootstrap-server kafka:9092 --create --if-not-exists --topic multimedia-retrieval-response --replication-factor 1 --partitions 1
      echo -e 'Successfully created the following topics:'
      kafka-topics --bootstrap-server kafka:9092 --list
      "
  kafka-ui:
    image: 'docker.io/provectuslabs/kafka-ui:v0.7.1'
    ports:
      - '8080:8080'
    networks:
      - 'mmir_net'
    environment:
      KAFKA_CLUSTERS_0_NAME: 'local-kafka'
      KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS: 'kafka:9092'
    depends_on:
      kafka:
        condition: service_started
  samba:
    image: 'ghcr.io/servercontainers/samba:smbd-only-a3.19.0-s4.18.9-r0'
    ports:
      - '445:445'
    volumes:
      - 'samba_data:/shares'
    networks:
      - 'mmir_net'
    environment:
      ACCOUNT_sambauser: samba
      UID_sambauser: 1000
      SAMBA_VOLUME_CONFIG_public: '[Public]; path=/shares; valid users = sambauser; admin users = sambauser; guest ok = yes; read only = no; browseable = yes; writeable = yes; public = yes; create mask = 0644; directory mask = 0755'
  postgres:
    image: 'docker.io/postgres:16.1-bookworm'
    volumes:
      - "postgres_data:/var/lib/postgresql/data"
    networks:
      - mmir_net
    environment:
      POSTGRES_DB: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_USER: postgres  
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      interval: 5s
      timeout: 15s
      retries: 5   
  pgadmin:
    image: docker.io/elestio/pgadmin:REL-8_2
    volumes:
      - 'pgadmin_data:/pgadmin4/servers.json'
    ports:
      - 8000:8080
    networks:
      - mmir_net
    environment:
      PGADMIN_DEFAULT_EMAIL: admin@email.com
      PGADMIN_DEFAULT_PASSWORD: admin
      PGADMIN_LISTEN_PORT: 8080
    depends_on:
      postgres:
        condition: service_healthy
  mmir_connector:
    image: '<mmir_connector_release_image>'
    networks:
      - mmir_net
    environment:
      API_KEY: '<api-key of gmaf>'
      GMAF_ADDRESS: 'http://<gmaf ip adress>'
      GMAF_PORT: '<gmaf port>'
    depends_on:
      kafka:
        condition: service_started
  web_app:
    image: '<web_app_release_image>'
    networks:
      - mmir_net
    ports:
      - '80:8080'
    depends_on:
      kafka:
        condition: service_started
      postgres:
        condition: service_healthy

networks:
  mmir_net:
    name: mmir_net
    driver: bridge

volumes:
  kafka_data:
    driver: local
  samba_data:
    driver: local
  postgres_data:
    driver: local
  pgadmin_data:
    driver: local
...
```

## Accessing the POC
Access the POC via a web browser by navigating to `http://localhost`.
Upon accessing the game, you will be presented with an interface that includes:
- A navigation bar with two section: `Instruction` and `Tactic`.
- A chessboard where the game is played.
### Demo
The user interface should display as shown below.
![demo](./doc/demo/demo.mov){width=80%}

## How to play
1. **Starting a game:** 
    - Once the interface loads, you are ready to play. You can begin by making your first move on the chessboard.

2. **Move suggestions:**
    - To receive move suggestions, click on a chess piece on the board.
    - The possible moves for that piece will be highlighted in green. 

3. **Instructional videos for chess pieces:**
    - Click on any chess piece.
    - On the `Instruction` section, click the `Search` button to view a video tutorial for the selected piece.

4. **Searching for strategic videos :**
    - On the `Tactic` section, enter the name of the chess tactic you want to learn about in the search bar.
    - Click the `Search` button to find instructional videos on that particular tactic.

## REST API documentation
The REST-API Documentation exposes endpoints of the POC, facilitating functionalities like video streaming for instructions of chess tactics and chess pieces. 

If the POC is gestarted:
  - The REST-API Documentation is available at the [Swagger UI](https://swagger.io/tools/swagger-ui/) page `http://localhost/swagger-ui/index.html`.
  - You can also find the documentation in JSON format at `http://localhost/api-docs` or in YAML format at `http://localhost/api-docs.yaml`.

The REST-API documentation in YAML format is represented as follow:
```yaml
---
openapi: 3.0.1
info:
  title: Chess Game Navigation API
  description: This API exposes endpoints of the chess game navigation.
  contact:
    name: Trang Nguyen
    email: huyentrang.nguyen@outlook.de
  version: "1.0"
servers:
- url: http://localhost
  description: Localhost Server URL
paths:
  /stream/tactic:
    get:
      tags:
      - video-streaming-controller
      operationId: getVideoTactic
      parameters:
      - name: searchQuery
        in: query
        required: true
        schema:
          type: string
      - name: clientId
        in: query
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: OK
          content:
            video/mp4:
              schema:
                type: string
                format: binary
        "404":
          description: Resource Not Found
        "408":
          description: Request Timeout
        "504":
          description: Gateway Timeout
  /stream/instruction:
    get:
      tags:
      - video-streaming-controller
      operationId: getVideoInstruction
      parameters:
      - name: searchQuery
        in: query
        required: true
        schema:
          type: string
      - name: clientId
        in: query
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: OK
          content:
            video/mp4:
              schema:
                type: string
                format: binary
        "404":
          description: Resource Not Found
        "408":
          description: Request Timeout
        "504":
          description: Gateway Timeout
components: {}
...
```