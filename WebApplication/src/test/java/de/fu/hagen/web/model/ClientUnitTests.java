package de.fu.hagen.web.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

public class ClientUnitTests {

    @Test
    void testClientCreation() {
        LocalDateTime creationTime = LocalDateTime.now();
        Client client = new Client(creationTime);

        assertNotNull(client);
        assertEquals(creationTime, client.getCreationTime());
    }

    @Test
    void testSetClientId() {
        Client client = new Client();
        Long clientId = 12L;

        client.setClientId(clientId);

        assertEquals(clientId, client.getClientId());
    }

    @Test
    void testSetCreationTime() {
        Client client = new Client();
        LocalDateTime creationTime = LocalDateTime.now();

        client.setCreationTime(creationTime);

        assertEquals(creationTime, client.getCreationTime());
    }
}
