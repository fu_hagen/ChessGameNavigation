package de.fu.hagen.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT) // This will start the Spring Boot application and make it available for test to perform requests to it.
class WebControllerIntegrationTests { 

	@LocalServerPort
  	private int port;
	
	@Autowired
    private TestRestTemplate restTemplate;

	@Test
	public void testHomeEndPoint() {
		// Perform the GET request for the home page
		ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://localhost:" + port, String.class);

        // Assert the response status code is OK (200)
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

}
