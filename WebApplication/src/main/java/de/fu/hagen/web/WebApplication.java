package de.fu.hagen.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The main class for starting the web application.
 */
@SpringBootApplication
@EnableScheduling
public class WebApplication {

	/**
     * The entry point for starting the web application.
     *
     * @param args Command-line arguments passed to the application (not used in this case).
     */
	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}

}
