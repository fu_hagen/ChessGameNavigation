package de.fu.hagen.web.broker.publisher;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

/**
 * Configuration class for setting up a Kafka producer using Spring Kafka.
 * This class defines beans for configuring the Kafka producer factory and KafkaTemplate.
 */
@Configuration
public class KafkaProducerConfig {

    // The Kafka broker's address
    private static final String BOOT_STRAP_ADDRESS = "kafka:9092";

    /**
     * Creates a Kafka Producer Factory bean with the specified configuration properties.
     *
     * @return A {@link ProducerFactory} for Kafka with String key and value serializers.
     */
    @Bean
    public ProducerFactory<String, String> producerFactory() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOT_STRAP_ADDRESS);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    } 

    /**
     * Creates a KafkaTemplate bean for producing messages to Kafka topics.
     *
     * @return A {@link KafkaTemplate} configured with the Kafka producer factory.
     */
    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }
}
