package de.fu.hagen.web.broker.listener;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

/**
 * Configuration class for setting up a Kafka consumer using Spring Kafka.
 * This class defines beans for configuring the Kafka consumer factory and listener container factory.
 */
@Configuration
public class KafkaConsumerConfig {

    // The Kafka broker's address
    private static final String BOOT_STRAP_ADDRESS = "kafka:9092";

    /**
     * Creates a Kafka Consumer Factory bean with the specified configuration properties.
     *
     * @return A {@link ConsumerFactory} for Kafka with String key and value deserializers.
     */
    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOT_STRAP_ADDRESS);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return new DefaultKafkaConsumerFactory<>(props);
    }

    /**
     * Creates a Concurrent Kafka Listener Container Factory bean for consuming Kafka messages.
     *
     * @return A {@link ConcurrentKafkaListenerContainerFactory} configured with the consumer factory.
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String>
    kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }
}
