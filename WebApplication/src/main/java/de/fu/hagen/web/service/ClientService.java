package de.fu.hagen.web.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import de.fu.hagen.web.model.Client;
import de.fu.hagen.web.model.ClientRepository;

/**
 * Service class for managing client-related operations.
 * This service provides methods for creating and deleting clients, as well as a scheduled task for cleaning up inactive clients.
 */
@Service
public class ClientService {

    private static final Long EXPIRED_TIME_IN_HOURS = 2L;

    private ClientRepository clientRepository;

    @Autowired
    public void setClientRepository (ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    /**
     * Creates and saves a new client with the current timestamp.
     *
     * @return The created Client instance.
     */
    public Client saveClient (){
        Client client = new Client(LocalDateTime.now());
        return clientRepository.save(client);
    }

    /**
     * Deletes a client by its unique identifier.
     *
     * @param clientId The unique identifier of the client to be deleted.
     */
    public void deleteClientById (Long clientId){
        clientRepository.deleteById(clientId);
    }

    /**
     * Scheduled task that runs every 30 minutes to clean up inactive clients.
     * Inactive clients are those created more than two hours ago.
     */
    @Scheduled(cron = "0 */30 * * * *")
    public void cleanupInactiveClients() {
        LocalDateTime twoHourAgo = LocalDateTime.now().minusHours(EXPIRED_TIME_IN_HOURS);
        List<Client> inactiveClients = clientRepository.findByCreationTimeBefore(twoHourAgo);
        clientRepository.deleteAll(inactiveClients);
    }
}
