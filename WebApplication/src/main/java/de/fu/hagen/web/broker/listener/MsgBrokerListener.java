package de.fu.hagen.web.broker.listener;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * Kafka message listener for processing responses from a multimedia retrieval service.
 * This class listens to a Kafka topic, extracts information from incoming JSON messages,
 * and stores them in a ConcurrentHashMap for later retrieval.
 */
@Component
public class MsgBrokerListener {
    
    // JSON keys
    private static final String KEY_ID = "id";
    private static final String KEY_MESSAGE = "message";

    // ConcurrentHashMap to store Kafka responses
    private ConcurrentHashMap<Long, String> kafkaResponses = new ConcurrentHashMap<Long, String>();

    /**
     * Kafka listener method that processes incoming messages from the specified topic.
     *
     * @param jsonMsg The JSON message received from the Kafka topic.
     */
    @KafkaListener(id = "WebApplication", topics = "multimedia-retrieval-response")
    public void listen(@Payload String jsonMsg) {
        JSONObject jsonObj = new JSONObject(jsonMsg);
        kafkaResponses.put(jsonObj.getLong(KEY_ID), jsonObj.getString(KEY_MESSAGE));
    } 

    /**
     * Retrieves the response message associated with a given client ID.
     *
     * @param clientId The unique client ID for which to retrieve the response message.
     * @return An Optional containing the response message if found, or empty if not found.
     */
    public Optional<String> getResponseStatus(final Long clientId) {
        if (kafkaResponses.containsKey(clientId)){
            return Optional.of(kafkaResponses.get(clientId));
        } 
        return Optional.empty();
    }  

    /**
     * Deletes the response element associated with a given client ID.
     *
     * @param clientId The unique client ID for which to delete the response element.
     */
    public void deleteResponseElement(final Long clientId){
        kafkaResponses.remove(clientId);
    } 
}
