package de.fu.hagen.web.samba;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import com.hierynomus.smbj.auth.AuthenticationContext;
import com.hierynomus.msdtyp.AccessMask;
import com.hierynomus.msfscc.FileAttributes;
import com.hierynomus.mssmb2.SMB2CreateDisposition;
import com.hierynomus.mssmb2.SMB2ShareAccess;
import com.hierynomus.smbj.SMBClient;
import com.hierynomus.smbj.connection.Connection;
import com.hierynomus.smbj.session.Session;
import com.hierynomus.smbj.share.DiskShare;
import com.hierynomus.smbj.share.File;

/**
 * Connector class for interacting with a Samba file server.
 * This class provides methods for reading and deleting files from a Samba share.
 */
public class FileserverConnector {

    // File server configurations
    private static final String DOMAIN = "WORKGROUP";
    private static final String SERVER_NAME = "samba";
    private static final String SHARE_NAME = "public";
    private static final String SAMBA_USER = "sambauser";
    private static final String SAMBA_PASSWORD = "samba";
    private static final String VIDEO_FILE_NAME = "video.mp4";

    private Long clientId;
    private SMBClient client;

    /**
     * Constructs a FileserverConnector instance with a client ID.
     *
     * @param clientId The unique identifier for the client.
     */
    public FileserverConnector(final Long clientId) {
        this.clientId = clientId;
        client = new SMBClient();
    } 

    /**
     * Reads a video file from the Samba share and returns it as a ByteArrayInputStream.
     *
     * @return A ByteArrayInputStream containing the video file's content, or an empty stream if an error occurs.
     */
    public ByteArrayInputStream readVideo() {
        try (Connection connection = client.connect(SERVER_NAME)) {
            Session session = connection.authenticate(
                new AuthenticationContext(SAMBA_USER, SAMBA_PASSWORD.toCharArray(), DOMAIN)
            );
            try (DiskShare diskShare = (DiskShare) session.connectShare(SHARE_NAME)) {
                Set<FileAttributes> fileAttributes = new HashSet<>();
                fileAttributes.add(FileAttributes.FILE_ATTRIBUTE_NORMAL);
                File file = diskShare.openFile(
                    new StringBuilder(String.valueOf(clientId)).append(java.io.File.separator).append(VIDEO_FILE_NAME).toString(), 
                    EnumSet.of(AccessMask.FILE_READ_DATA), 
                    fileAttributes, 
                    SMB2ShareAccess.ALL, 
                    SMB2CreateDisposition.FILE_OPEN, 
                    null
                );
                // https://www.baeldung.com/convert-input-stream-to-a-file
                return new ByteArrayInputStream(file.getInputStream().readAllBytes());
            } 
        } 
        catch (IOException e) {
            e.printStackTrace();
        }

        return new ByteArrayInputStream(new byte[0]); // Empty ByteArrayInputStream
    } 

    /**
     * Deletes a video file from the Samba share.
     */
    public void deleteVideo() {
        try (Connection connection = client.connect(SERVER_NAME)) {
            Session session = connection.authenticate(
                new AuthenticationContext(SAMBA_USER, SAMBA_PASSWORD.toCharArray(), DOMAIN)
            );
            try (DiskShare diskShare = (DiskShare) session.connectShare(SHARE_NAME)) {
                diskShare.rmdir(
                    new StringBuilder(String.valueOf(clientId)).toString(), 
                    true
                );
            } 
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
    } 

}
