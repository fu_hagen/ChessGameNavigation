package de.fu.hagen.web.model;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for managing {@link Client} entities.
 * This repository provides methods to interact with the database and perform common CRUD (Create, Read, Update, Delete) operations on client data.
 * In addition, it defines a custom query method for retrieving clients created before a specified timestamp.
 */
@Repository
public interface ClientRepository extends CrudRepository<Client, Long>{

    /**
     * Retrieves a list of clients created before the specified timestamp.
     *
     * @param creationTime The timestamp representing the cutoff time for client creation.
     * @return A list of {@link Client} entities that were created before the given timestamp.
     */
    List<Client> findByCreationTimeBefore(LocalDateTime creationTime);
}
