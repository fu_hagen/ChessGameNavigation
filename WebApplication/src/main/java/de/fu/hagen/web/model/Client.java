package de.fu.hagen.web.model;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

/**
 * Entity class representing a client in the application.
 * This class is annotated as an entity, making it a persistent object in the database.
 */
@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "creation_time")
    private LocalDateTime creationTime;

    /**
     * Default constructor for the Client entity.
     */
    public Client(){
        
    }

    /**
     * Constructor for creating a Client entity with a specified creation time.
     *
     * @param creationTime The timestamp representing the creation time of the client.
     */
    public Client(LocalDateTime creationTime){
        this.creationTime=creationTime;
    }

    /**
     * Get the unique identifier of the client.
     *
     * @return The client's unique identifier.
     */
    public Long getClientId() {
        return this.clientId;
    }

    /**
     * Set the unique identifier of the client.
     *
     * @param clientId The client's unique identifier.
     */
    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    /**
     * Get the timestamp representing the creation time of the client.
     *
     * @return The timestamp of the client's creation time.
     */
    public LocalDateTime getCreationTime() {
        return this.creationTime;
    }

    /**
     * Set the timestamp representing the creation time of the client.
     *
     * @param creationTime The timestamp of the client's creation time.
     */
    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

}
