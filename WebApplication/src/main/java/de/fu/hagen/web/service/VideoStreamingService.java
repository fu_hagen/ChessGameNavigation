package de.fu.hagen.web.service;

import java.io.ByteArrayInputStream;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import de.fu.hagen.web.broker.KafkaResultPoller;
import de.fu.hagen.web.broker.listener.MsgBrokerListener;
import de.fu.hagen.web.broker.publisher.MsgBrokerPublisher;
import de.fu.hagen.web.model.ClientRepository;
import de.fu.hagen.web.samba.FileserverConnector;
import reactor.core.publisher.Mono;

/**
 * Service for streaming video files.
 * This service uses Spring's ResourceLoader to efficiently load video files
 * from the classpath. It is designed to work asynchronously using Project Reactor's Mono type.
 */
@Service
public class VideoStreamingService {

    private static final String MMIR_SUCCESS = "success";
    private static final String MMIR_FAIL = "fail";
    private static final String ERROR_RESOURCE_NOT_FOUND_BODY = "classpath:err_body/error404.png";
    private static final String ERROR_REQUEST_TIMEOUT_BODY = "classpath:err_body/error408.png";
    private static final String ERROR_GATEWAY_TIMEOUT_BODY = "classpath:err_body/error504.png";

    private ClientRepository repository;
    private ResourceLoader resourceLoader;
    private MsgBrokerPublisher publisher;
    private ThreadPoolTaskExecutor taskExecutor;
    private MsgBrokerListener listener;
    
    @Autowired
    public void setRepository(ClientRepository repository) {
        this.repository = repository;
    }

    @Autowired
    public void setResourceLoader (ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Autowired
    public void setPublisher (MsgBrokerPublisher publisher) {
        this.publisher = publisher;
    }

    @Autowired
    public void setTaskExecutor (ThreadPoolTaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @Autowired
    public void setListener (MsgBrokerListener listener) {
        this.listener = listener;
    }

    /**
     * Retrieves a video resource as a Mono based on its name.
     * 
     * @param searchQuery The search query from the client (passed as a query parameter).
     * @param clientId The unique identifier for the client making the request (passed as a query parameter).
     * @return A Mono of Resource that represents the video file.
     *         The video file is loaded from the classpath.
     */
    public Mono<Resource> getVideo(String searchQuery, Long clientId) {

        if ( !repository.findById(clientId).isPresent() ) {
            return Mono.fromSupplier(
                () -> {
                    return resourceLoader.getResource( ERROR_REQUEST_TIMEOUT_BODY );
                }
            );
        }

        publisher.sendToKafka(clientId, searchQuery);

        Future<Optional<String>> result = taskExecutor.submit(
            new KafkaResultPoller(listener, clientId)
        );

        try {
            Optional<String> kafkaResponseResult = result.get();
            if (kafkaResponseResult.isPresent()) {
                if (kafkaResponseResult.get().equals(MMIR_FAIL)) {
                    return Mono.fromSupplier(
                        () -> {
                            return resourceLoader.getResource(ERROR_RESOURCE_NOT_FOUND_BODY);
                        } 
                    );
                } 
                if ( !kafkaResponseResult.get().equals(MMIR_SUCCESS) ) {
                    System.err.println(
                        new StringBuilder("Response message from kafka is either ").append(MMIR_SUCCESS).append(" or ").append(MMIR_FAIL).append("! Please redesign the API!")
                    );
                    return Mono.fromSupplier(
                        () -> {
                            return resourceLoader.getResource(ERROR_GATEWAY_TIMEOUT_BODY);
                        } 
                    );
                } 
                FileserverConnector fileserverConnector = new FileserverConnector(clientId);
                ByteArrayInputStream inStream = fileserverConnector.readVideo();
                fileserverConnector.deleteVideo();
                return Mono.fromSupplier(
                    () -> { 
                        return new InputStreamResource(inStream);
                    }
                );
            }
            else {
                return Mono.empty();
            }  
        } 
        catch (InterruptedException e) {
            e.printStackTrace();
        } 
        catch (ExecutionException e) {
            e.printStackTrace();
        }
        
        return Mono.empty();
    }

    
}
