package de.fu.hagen.web.config;

import de.fu.hagen.web.service.VideoStreamingService;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * Configuration class for setting up web router functions.
 * This class defines the router functions for handling specific URL endpoints in the web application.
 * It is annotated as a configuration class in Spring's application context.
 */
@Configuration
public class EndPointConfig {

    private VideoStreamingService service;

    @Autowired
    public void setVideoStreamingService (VideoStreamingService service) {
        this.service = service;
    }

    /**
     * Creates and registers a router function for handling video instruction endpoints.
     * This method sets up a GET route for streaming video instruction files, directing requests to a handler method.
     * 
     * @return A RouterFunction that maps the GET request to the appropriate handler.
     * @see #videoInstructionHandler(ServerRequest)
     */
    @Bean
    public RouterFunction<ServerResponse> routerInstruction(){
        return RouterFunctions.route().GET("/instruction", RequestPredicates.queryParam("name", Objects::nonNull).and(RequestPredicates.queryParam("clientId", Objects::nonNull)), this::videoInstructionHandler).build();
    }

    /**
     * Creates and registers a router function for the handling video tactic endpoint.
     * This method establishes a GET route for handling requests related to streaming tactic videos, directing them to a handler method.
     *
     * @return A RouterFunction mapping the GET request to the appropriate handler.
     * @see #videoTacticHandler(ServerRequest)
     */
    @Bean
    public RouterFunction<ServerResponse> routerTactic(){
        return RouterFunctions.route().GET("/tactic", RequestPredicates.queryParam("name", Objects::nonNull).and(RequestPredicates.queryParam("clientId", Objects::nonNull)), this::videoTacticHandler).build();
    }


    /**
     * Handles GET requests for streaming video instruction files.
     * This method processes requests to stream a video file based on the provided name in the URL path variable.
     * It retrieves the video resource and prepares it for streaming in the response.
     *
     * @param serverRequest The request containing the name of the video file as a query parameter
     * @return A Mono of ServerResponse streaming the requested video file.
     * @see VideoStreamingService#getVideo(String, Long)
     */
    private Mono<ServerResponse> videoInstructionHandler(ServerRequest serverRequest){
        String name = serverRequest.queryParam("name").orElse("");
        String clientId = serverRequest.queryParam("clientId").orElse("");
        return ServerResponse.ok().contentType(MediaType.valueOf("video/mp4")).body(this.service.getVideo(name, Long.parseLong(clientId)), Resource.class);
    }

    /**
     * Handles GET requests related to tactics.
     * This method processes requests for tactics and responds with the corresponding video resource.
     *
     * @param serverRequest The request containing the tactic name and clientId as query parameters.
     * @return A Mono of ServerResponse streaming the requested video file.
     * @see VideoStreamingService#getVideo(String, Long)
     */
    private Mono<ServerResponse> videoTacticHandler(ServerRequest serverRequest){
        String name = serverRequest.queryParam("name").orElse("");
        String clientId = serverRequest.queryParam("clientId").orElse("");
        return ServerResponse.ok().contentType(MediaType.valueOf("video/mp4")).body(this.service.getVideo(name, Long.parseLong(clientId)), Resource.class);
    }

}

