package de.fu.hagen.web.broker.publisher;

import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

/**
 * Kafka message publisher for sending messages to a multimedia retrieval service.
 * This class provides methods to create and send JSON messages to a Kafka topic.
 */
@Component
public class MsgBrokerPublisher {
    
    // JSON keys
    private static final String KEY_ID = "id";
    private static final String KEY_MESSAGE = "message";

    // Kafka topic name
    private static final String TOPIC_NAME = "multimedia-retrieval";

    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public void setKafkaTemplate (KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }
    
    /**
     * Sends a JSON message to the specified Kafka topic in the non-blocking way.
     *
     * @param key The unique identifier for the message.
     * @param msg The message content to be sent.
     */
    public void sendToKafka(final Long key, final String msg) {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put(KEY_ID, key);
        jsonObj.put(KEY_MESSAGE, msg);
        CompletableFuture<SendResult<String, String>> future = kafkaTemplate.send(TOPIC_NAME, jsonObj.toString());
        future.whenComplete(
            (SendResult<String, String> result, Throwable ex) -> {
                if (ex != null) {
                    System.err.println(
                        new StringBuilder("The message ").append(jsonObj.toString()).append(" cannot be sent.").append(" Because ").append(ex.getMessage())
                    );
                } 
            } 
        );
    } 
}
