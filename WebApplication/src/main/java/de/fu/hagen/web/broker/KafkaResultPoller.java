package de.fu.hagen.web.broker;

import java.util.Optional;
import java.util.concurrent.Callable;

import de.fu.hagen.web.broker.listener.MsgBrokerListener;

/**
 * Task for polling Kafka result responses associated with a client ID.
 * This class retrieves response messages from a {@link MsgBrokerListener} and provides
 * a mechanism for polling and waiting for the response within a specified number of retries.
 */
public class KafkaResultPoller implements Callable<Optional<String>> {

    private MsgBrokerListener msgBrokerListener;
    private Long clientId;
    
    // Constants for controlling polling behavior
    private static final Long SLEEP_TIME = 50L;
    private static final int RETRIES = 100;

    /**
     * Constructs a KafkaResultPoller with a {@link MsgBrokerListener} and a client ID.
     *
     * @param msgBrokerListener The listener used to retrieve response messages.
     * @param clientId The unique client ID for which to poll the response.
     */
    public KafkaResultPoller(MsgBrokerListener msgBrokerListener, final Long clientId) {
        this.msgBrokerListener = msgBrokerListener;
        this.clientId = clientId;
    } 

    /**
     * Polls and retrieves the response associated with the client ID.
     * This method repeatedly checks for a response message from the listener,
     * and it sleeps for a short duration between retries.
     *
     * @return An {@link Optional} containing the response message if found,
     *         or empty if the maximum number of retries is reached without a response.
     * @throws InterruptedException If the thread is interrupted while sleeping.
     */
    @Override
    public Optional<String> call() throws Exception {
        for (int i = 0; i < RETRIES; i++){
            Optional<String> result = msgBrokerListener.getResponseStatus(clientId);
            if (result.isPresent()) {
                msgBrokerListener.deleteResponseElement(clientId);
                return result;
            } 
            Thread.sleep(SLEEP_TIME);
        } 

        return Optional.empty();
    }
    
}
