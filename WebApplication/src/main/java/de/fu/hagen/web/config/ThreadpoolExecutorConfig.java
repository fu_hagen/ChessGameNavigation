package de.fu.hagen.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Configuration class for defining a ThreadPoolTaskExecutor bean.
 * This class configures a thread pool executor for managing concurrent tasks in a Spring application.
 */
@Configuration
public class ThreadpoolExecutorConfig {

    // Constants for configuring the thread pool
    private static final int CORE_POOL_SIZE = 20;
    private static final int MAX_POOL_SIZE = 40;

    /**
     * Creates and configures a ThreadPoolTaskExecutor bean.
     *
     * @return A {@link ThreadPoolTaskExecutor} configured with core and max pool sizes,
     *         and the option to wait for tasks to complete on shutdown.
     */
    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(CORE_POOL_SIZE);
        pool.setMaxPoolSize(MAX_POOL_SIZE);
        pool.setWaitForTasksToCompleteOnShutdown(true);
        return pool;
    }
    
}
