package de.fu.hagen.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;

@OpenAPIDefinition
@Configuration
public class OpenAPIConfig {

    @Bean
    public OpenAPI baseOpenAPI() {
        Server localServer = new Server();
        localServer.url("http://localhost");
        localServer.description("Localhost Server URL");

        Contact contact = new Contact();
        contact.email("huyentrang.nguyen@outlook.de");
        contact.name("Trang Nguyen");

        Info info = new Info()
            .title("Chess Game Navigation API")
            .version("1.0")
            .contact(contact)
            .description("This API exposes endpoints of the chess game navigation.");

    return new OpenAPI().info(info).addServersItem(localServer);
  }
}
