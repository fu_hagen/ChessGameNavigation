package de.fu.hagen.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import de.fu.hagen.web.service.ClientService;

/**
 * Controller handling web page rendering and views.
 * This controller is responsible for processing requests related to the web application's
 * user interface. It manages navigation and data presentation on the web pages.
 */
@Controller
@RequestMapping("/")
public class WebController {
    
    private ClientService clientService;

    @Autowired
    public void setClientService (ClientService clientService) {
        this.clientService = clientService;
    } 

    /**
     * Handles requests to the home page of the application.
     * This method processes GET requests to the root URL ('/') and prepares the necessary
     * model attributes for rendering the home page view. It sets flags for UI control and
     * returns the name of the view to be rendered.
     * 
     * @param model The model object used to pass data to the view.
     * @return The view name to be rendered for the home page.
     */
    @GetMapping(value = "/") 
    public String home(Model model) {
        // Instruction section is not initially active when the home page is loaded.
        model.addAttribute("isInstructionActive", false);

        // Tactic section is not initially active when the home page is loaded.
        model.addAttribute("isTacticActive", false);

        // Save a new client and retrieve its ID.
        Long clientId = clientService.saveClient().getClientId();

        // Pass clientId to view
        model.addAttribute("clientId", clientId);

        return "home"; 
    }
} 