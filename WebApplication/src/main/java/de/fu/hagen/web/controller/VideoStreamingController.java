package de.fu.hagen.web.controller;

import de.fu.hagen.web.service.VideoStreamingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Controller for handling video streaming requests.
 * This controller provides API endpoints for streaming video content related to chess instructions
 * and tactics. It collaborates with the {@link serviceVideoStreamingService} to retrieve and stream video files
 * based on the search query and associated client IDs.
 */
@RestController
@RequestMapping("/")
public class VideoStreamingController {
    
    private VideoStreamingService streamingService;

    @Autowired 
    public void setVideoStreamingController (VideoStreamingService streamingService) {
        this.streamingService = streamingService;
    }

    /**
     * Endpoint for streaming a video file containing chess piece instructions.
     * This method responds to GET requests and streams the requested video file. The search query
     * is passed as a query parameter along with the client ID. It expects the video to be available and readable.
     *
     * @param searchQuery The search query from the client (passed as a query parameter).
     * @param clientId The unique identifier for the client making the request (passed as a query parameter).
     * @return A Mono of Resource representing the video file, ready to be streamed.
     */
    @GetMapping(value = "stream/instruction", produces = "video/mp4")
    public Mono<Resource> getVideoInstruction(@RequestParam String searchQuery, @RequestParam Long clientId) {
        return streamingService.getVideo(searchQuery, clientId);
    }

     /**
     * Endpoint for streaming a video file containing chess tactics.
     * This method responds to GET requests and streams the requested video file for chess tactics.
     * The search request is passed as a query parameter along with the client ID. It expects the video
     * to be available and readable.
     *
     * @param searchQuery The search query from the client (passed as a query parameter).
     * @param clientId The unique identifier for the client making the request (passed as a query parameter).
     * @return A Mono of Resource representing the video file, ready to be streamed.
     */
    @GetMapping(value = "stream/tactic", produces = "video/mp4")
    public Mono<Resource> getVideoTactic(@RequestParam String searchQuery, @RequestParam Long clientId) {
        return streamingService.getVideo(searchQuery, clientId);
    }
}



