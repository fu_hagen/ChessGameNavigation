
/**
 * Toggle the 'active' class on buttons based on the provided ID.
 * @param {string} id - The ID of the button to be toggled as active.
 */
function toggleActive(id) {
    // Remove 'active' class from both buttons
    document.getElementById('instruction').classList.remove('active');
    document.getElementById('tactic').classList.remove('active');

    // Add 'active' class to the clicked button
    document.getElementById(id).classList.add('active');
    
}


/**
 * Change the background color of a button when clicked and revert it on release.
 * 
 * @param {string} buttonId - The ID of the button to handle color change.
 */
function changeColorOnClick(buttonId) {
    const button = document.getElementById(buttonId);

    // Change color on mouse down (button is pressed)
    button.addEventListener('mousedown', function () {
        button.style.backgroundColor = 'darkgrey'; // Change to the desired color
    });

    // Revert color on mouse up (button is released)
    button.addEventListener('mouseup', function () {
        button.style.backgroundColor = ''; // Revert to the original color 
    });
}

// Call the function for the 'Get' button
changeColorOnClick('searchInstruction');

// Call the function for the 'Search' button
changeColorOnClick('searchTactic');


let selectedBoxId;
let selectedBoxText;

/**
 * Handle the click event on chessboard boxes.
 * 
 * @param {string} boxId - The ID of the box.
 * @param {string} piece - The type of chess piece on the box.
 */
function handleBoxClick(boxId, piece) {
    // Check if the box has a pink background
    if (document.getElementById(boxId).style.backgroundColor === 'pink') {
        selectedBoxId = boxId;      
        selectedBoxText = piece;   
    }
    else {
        selectedBoxId = null;
        selectedBoxText = null;
    }
}

// Add click event handling to each chessboard box
let chessBoxes = document.querySelectorAll('.box');
chessBoxes.forEach(function (box) {
    box.addEventListener('click', function () {
        // Extract box ID and piece type that stands on the box
        var boxId = box.id;
        var piece = box.innerText.trim();

        // Call the handling function 
        handleBoxClick(boxId, piece);
    });
});


/**
 * Load an instruction video for a selected chess piece.
 * 
 * @param {string} selectedBoxText - The type of the selected chess piece.
 */
function loadVideoForPiece(selectedBoxText) {

    toggleActive('searchInstruction');
    
    // Alert the user if a chess piece is not selected
    if (!selectedBoxText) {
        alert("Please select a chess piece in your turn for searching instruction.");
        return;
    }

    // Remove the first letter from the value of selectedBoxText.
    // For example: 'Wpawn' is converted to 'pawn'
    let modifiedText = selectedBoxText.slice(1);
    
    // The video URL for streaming
    let clientId = document.querySelector('[data-clientId]').getAttribute('data-clientId');
    let videoUrl = `/stream/instruction?searchQuery=${modifiedText}&clientId=${clientId}`;


    // Fetch the video and update the popup
    let videoPopup = document.getElementById('videoPopup');
    let videoElement = document.getElementById('chessTutorial');
    videoElement.src = videoUrl;
    videoElement.load();
    videoPopup.style.display = 'block';
    
}


/**
 * Load an instruction video for a chess tactic based on the tactic name.
 */
function loadTacticVideo() {
    // Get the tactic name from the input field
    let tacticName = document.getElementById('tacticInput').value.trim();

    // Alert the user if the input field is empty
    if (!tacticName) {
        alert("Please enter a tactic name before searching.");
        return;
    }

    //The URL for the video stream 
    let clientId = document.querySelector('[data-clientId]').getAttribute('data-clientId');
    let videoUrl = `/stream/tactic?searchQuery=${tacticName}&clientId=${clientId}`;

    // Fetch the video and update the popup
    let videoPopup = document.getElementById('videoPopup');
    let videoElement = document.getElementById('chessTutorial');
    videoElement.src = videoUrl;
    videoElement.load(); // Load the new video source
    videoPopup.style.display = 'block'; // Show the video popup
}

// Listen for the keydown event on the input field
document.getElementById('tacticInput').addEventListener('keydown', function (event) {
    // Check if the "Enter" key is pressed
    if (event.key === 'Enter') {
        // Call the loadTacticVideo function when the "Enter" key is pressed
        loadTacticVideo();
    }
});


/**
 * Close the video popup.
 */
function closePopup() {
    let videoPopup = document.getElementById('videoPopup');
    let video = document.getElementById('chessTutorial');
    video.pause();
    videoPopup.style.display = 'none';
}
