# Web Application
This service act as the entry point for web clients.
- [Description](#description)
- [Tech Stack](#tech-stack)
- [Build the project](#build-the-project)
- [Native system](#native-system)
- [Developing inside a container](#developing-inside-a-container)
- [Containerizing the service](#containerizing-the-service)
- [Run the project](#run-the-project)
- [Tests of the project](#tests-of-the-project)
- [Service interaction](#service-interaction)
- [Release](#release)

## Description
This service acts as the entry point, handling incoming HTTP requests and providing responses. It is connected with the user interface and manages user interactions with
the system. The Web Application is designed based on the [Model-View-Controller (MVC)](https://developer.mozilla.org/en-US/docs/Glossary/MVC) design pattern. It incorporates specialized connectors that facilitate data transfer and communication with Kafka messages broker and file server.

## Tech Stack
This project is developed using the following framework and libraries:
- [Thymeleaf](https://www.thymeleaf.org/): For rendering server-side HTML templates.
- [Spring WebFlux](https://docs.spring.io/spring-framework/reference/web/webflux.html): For building asynchronous, non-blocking web applications.
- [Spring boot Kafka](https://docs.spring.io/spring-boot/docs/current/reference/html/messaging.html#messaging.kafka): For communicating with the Kafka messages broker.
- [SMBJ](https://github.com/hierynomus/smbj): For multimedia data download from the file server.
- [HTML](https://developer.mozilla.org/en-US/docs/Web/HTML?retiredLocale=de), [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS), [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript): For structuring, styling and adding functionality to the navigation system part.
- [Framework for chessboard](https://github.com/jahid28/Games/tree/main/CHESS): For structuring, styling and adding chess logic to the chessboard part. No license is specified.

## Build the project
The build and dependencies management tool of this project is [Maven](https://maven.apache.org/). Maven Build Lifecycle can be found [here](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html). 

### Native system
This project only requires [Maven >= 3.9.5](https://maven.apache.org/install.html) and [JDK-21](https://adoptium.net/de/temurin/releases/) to build. If you do not want to install Maven on your system, you can also use the local Maven installation coming with this project:
- `mvnw`: on Linux machines.
- `mvnw.cmd`: on Windows machines. 

### Developing inside a container
Is is also possible to build this project inside a container. For this purpose, the `docker.io/maven:3.9.6-eclipse-temurin-21-jammy` Docker image can be used. If [Visual Studio Code](https://code.visualstudio.com/) is the chosen IDE, you can create '.devcontainer/devcontainer.json' in the root folder of this project with the following contents:

```json
{
	"name": "Web Application",

	"image": "docker.io/maven:3.9.6-eclipse-temurin-21-jammy",

	"customizations": {
		"vscode": {
			"extensions": [
				"vmware.vscode-boot-dev-pack",
				"vscjava.vscode-java-pack",
				"sohibe.java-generate-setters-getters"
			]
		}
	},

	"runArgs": [
		"--network=mmir_net"
	]
}
```

### Containerizing the service
Running services inside containers is the state of the art of server software development. All required dependencies will be packed inside the container and managing those will be very convenience. To containerize this service, one should first compile the project with Maven. Afterwards, the following command the to be entered:

```
docker build -t "<image_name>:<image_tag>" .
```

- `image_name` is a String and can be anything.
- `image_tag` is also a String and may follow [Semantic Versioning](https://semver.org/). 

## Run the project
After compiling this project with Maven, you can start the service using `mvn spring-boot:run`. For more information please visit [this article](https://spring.io/guides/gs/spring-boot/).

## Tests of the project
To improve the code quality, the author also includes unit tests and integration tests in this project. To run the integration tests you need to set up a Kafka instance. For the convenience, the author recommend the development inside the container (s. [Developing inside a container](#developing-inside-a-container)) and the usage of [Docker Compose](https://docs.docker.com/compose/). The contents of the `docker-compose` file can be as follows:

```yaml
---
version: "3"

services:
  zookeeper:
    image: 'confluentinc/cp-zookeeper:7.5.3'
    networks:
      - 'mmir_net'
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
  kafka:
    image: 'confluentinc/cp-kafka:7.5.3'
    networks:
      - 'mmir_net'
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: 'zookeeper:2181'
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka:9092
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: '1'
    depends_on:
      - 'zookeeper'

networks:
  mmir_net:
    name: mmir_net
    driver: bridge
...
```

There is also a Continuous Integration (CI) [pipeline](https://gitlab.com/fu_hagen/ChessGameNavigation/-/blob/main/.gitlab-ci.yml?ref_type=heads) for this project, which covers the automatic testing. For more information about Gitlab CI, please visit [this documentation](https://docs.gitlab.com/ee/ci/).

## Service interaction
To interact with the Web Application, one should use a web browser (e. g. Firefox). Multimedia data will be displayed in the browser.

To test interact with the Web Application, one can start the service inside the container (as mentioned above) and run the following `Docker Compose set up`:

```yaml
---
version: "3"

services:
  zookeeper:
    image: 'confluentinc/cp-zookeeper:7.5.3'
    networks:
      - 'mmir_net'
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
  kafka:
    image: 'confluentinc/cp-kafka:7.5.3'
    networks:
      - 'mmir_net'
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: 'zookeeper:2181'
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka:9092
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: '1'
    depends_on:
      zookeeper:
        condition: service_started
  init-kafka:
    image: 'confluentinc/cp-kafka:7.5.3'
    depends_on:
      kafka:
        condition: service_started
    networks:
      - 'mmir_net'
    entrypoint: [ '/bin/sh', '-c' ]
    command: |
      "
      # blocks until kafka is reachable
      kafka-topics --bootstrap-server kafka:9092 --list

      echo -e 'Creating kafka topics'
      kafka-topics --bootstrap-server kafka:9092 --create --if-not-exists --topic multimedia-retrieval --replication-factor 1 --partitions 1
      kafka-topics --bootstrap-server kafka:9092 --create --if-not-exists --topic multimedia-retrieval-response --replication-factor 1 --partitions 1
      echo -e 'Successfully created the following topics:'
      kafka-topics --bootstrap-server kafka:9092 --list
      "
  kafka-ui:
    image: 'docker.io/provectuslabs/kafka-ui:v0.7.1'
    ports:
      - '8080:8080'
    networks:
      - 'mmir_net'
    environment:
      KAFKA_CLUSTERS_0_NAME: 'local-kafka'
      KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS: 'kafka:9092'
    depends_on:
      kafka:
        condition: service_started
  samba:
    image: 'ghcr.io/servercontainers/samba:smbd-only-a3.19.0-s4.18.9-r0'
    ports:
      - '445:445'
    networks:
      - 'mmir_net'
    environment:
      ACCOUNT_sambauser: samba
      UID_sambauser: 1000
      SAMBA_VOLUME_CONFIG_public: '[Public]; path=/shares; valid users = sambauser; admin users = sambauser; guest ok = yes; read only = no; browseable = yes; writeable = yes; public = yes; create mask = 0644; directory mask = 0755'
  postgres:
    image: 'docker.io/postgres:16.1-bookworm'
    networks:
      - mmir_net
    environment:
      POSTGRES_DB: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_USER: postgres  
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      interval: 5s
      timeout: 15s
      retries: 5   
  pgadmin:
    image: docker.io/elestio/pgadmin:REL-8_2
    ports:
      - 8000:8080
    networks:
      - mmir_net
    environment:
      PGADMIN_DEFAULT_EMAIL: admin@email.com
      PGADMIN_DEFAULT_PASSWORD: admin
      PGADMIN_LISTEN_PORT: 8080
    depends_on:
      postgres:
        condition: service_healthy
  mmir_connector:
    image: '<MMIR-Connector-Image>'
    networks:
      - mmir_net
    environment:
      API_KEY: '<api-key of gmaf>'
      GMAF_ADDRESS: 'http://<gmaf ip adress>'
      GMAF_PORT: '<gmaf port>'
    depends_on:
      kafka:
        condition: service_started

networks:
  mmir_net:
    name: mmir_net
    driver: bridge
...
```
In order for Web Application to work properly, the following environment variables have to be set:
- `API_KEY`: the api key to authenticate to GMAF.
- `GMAF_ADDRESS`: the IP adress of the GMAF instance.
- `GMAF_PORT`: the TCP port of the GMAF instance.

These informations are managed by the [Chair of Multimedia and Internet Applications of the University of Hagen](https://www.fernuni-hagen.de/multimedia-internetanwendungen/index.shtml).

Web Application publishes messages to the Kafka topic `multimedia-retrieval`. These messages are only consumed by [MMIR Connector](https://gitlab.com/fu_hagen/ChessGameNavigation/-/tree/main/MmirConnector?ref_type=heads) and can have the following form:

`key`: nothing.

`value`:
```json
{
	"id": "<client id>",
	"message": "<search query>"
}
```
Explanation of message value:
- `id` (Long): unique id of the client connecting to the Web Application.
- `message` (String): a search query that can be anything (e. g. skewers, pins).

From [MMIR Connector](https://gitlab.com/fu_hagen/ChessGameNavigation/-/tree/main/MmirConnector?ref_type=heads) published messages to the Kafka topic `multimedia-retrieval-response` are consumed by Web Application and can have the following form:

`key`: nothing.

`value`:
```json
{
	"id": "<client id>",
	"message": "success|fail"
}
```

Explanation of message value:
- `id` (Long): unique id of the client connecting to the Web Application.
- `message` (String): can be success or fail. This indicates if the multimedia data is retrieved from GMAF or not.

Furthermore, one can inspect the multimedia data on the file server. The `id` mentioned above is the name of the folder containing the multimedia data.


## Release
Releases of this service are automatically built using Continuous Integration and Continuous Delivery technologies of Gitlab. All releases can be found on the [release page](https://gitlab.com/fu_hagen/ChessGameNavigation/-/releases).